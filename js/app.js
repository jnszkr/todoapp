// application
var App = defineClass({
        init: function (state) {
            // set state
            this.state = state || {list: {items: []}, input: {}};

            // ui components
            this.input = new Input(this.state.input).onReturnPressed(this._addItem.bind(this));
            this.button = new Button({innerText: "Add"}).onClick(this._addItem.bind(this));
            this.list = new TodoList(this.state.list);

        },
        // add new item to the list
        _addItem: function (item) {
            var text = this.input.getState().value;

            this.list.addItem({text: text, done: false});
            this.input.setState({value: ""});
            this.input.focus();
        },
        // render function
        render: function () {
            var el = document.createElement("div"),
                header = document.createElement("div");

            header.classList.add("header");
            header.appendChild(this.input.render());
            header.appendChild(this.button.render());

            el.appendChild(header);
            el.appendChild(this.list.render());

            return el;
        }
    }
);


