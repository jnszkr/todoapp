// button
var Button = defineClass({
    // subscribe to click
    onClick: function (callback) {
        this._clickCallback = callback;
        return this;
    },
    _onClick: function (e) {
        if (typeof this._clickCallback === "function") {
            this._clickCallback(e);
        }
    },
    render: function () {
        var el = document.createElement("button");
        el.innerText = this.state.innerText || "";
        el.onclick = this._onClick.bind(this);
        el.classList.add("button");
        return el;
    }
});

