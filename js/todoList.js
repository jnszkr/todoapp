// todo list
var TodoList = defineClass({
    init: function (state) {
        this.state = state || {items: []};
    },
    addItem: function (item) {
        this.setState({items: [item].concat(this.state.items)});
    },
    removeItem: function (item) {
        var i = this.state.items.indexOf(item);
        if (i >= 0) {
            this.state.items.splice(i, 1);
            this.setState(this.state);
        }
    },
    render: function () {
        var self = this,
            todoItem,
            el = document.createElement('ul');

        el.classList.add("todo-list");

        this.state.items.map(function (item) {
            todoItem = new TodoItem(item).onRemove(self.removeItem.bind(self, item));
            el.appendChild(todoItem.render());
        });

        return el;
    }
});

