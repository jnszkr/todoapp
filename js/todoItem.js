// item

var TodoItem = defineClass({
    _checkboxClick: function (e) {
        e.stopPropagation();
        this.setState({done: !this.state.done});
        return this;
    },
    _remove: function (e) {
        e.stopPropagation();
        if (this.onRemoveCallback) {
            this.onRemoveCallback(this.state);
        }
    },
    // subscribe to remove click
    onRemove: function (callback) {
        this.onRemoveCallback = callback;
        return this;
    },
    render: function () {
        var el = document.createElement("li"),
            checkbox = document.createElement("div"),
            text = document.createElement("div"),
            rem = document.createElement("div");

        el.classList.add('todo-item');

        text.innerHTML = this.state.text;
        text.classList.add('text');

        checkbox.classList.add('checkbox');

        if (this.state.done) {
            el.classList.add('done');
        }

        el.onclick = this._checkboxClick.bind(this);

        rem.onclick = this._remove.bind(this);
        rem.classList.add('remove');
        rem.innerText = "X";

        el.appendChild(checkbox);
        el.appendChild(text);
        el.appendChild(rem);

        return el;
    }
});