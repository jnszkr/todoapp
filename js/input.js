
// input field
var Input = defineClass({
     _onInput: function(e) {
        var value = e.target.value;
        this.setState({value: value}, true);
    },
    _onKeypress: function (e) {
        if (e.keyCode === 13) {
            if (typeof this._returnPressedCallback === "function") {
                this._returnPressedCallback(this.state);
            }
        }
    },
    // subscribe to return key pressed
    onReturnPressed: function (callback) {
        this._returnPressedCallback = callback;
        return this;
    },
    // set focus
    focus: function () {
        this.el.focus();
    },
    render: function () {
        var el = document.createElement('input');
        el.autofocus = true;
        el.value = this.state.value;
        el.oninput = this._onInput.bind(this);
        el.onkeypress = this._onKeypress.bind(this);
        el.classList.add("input");
        return el;
    }
});
