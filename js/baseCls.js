// define class

function defineClass(cls) {

    function baseCls() {
        this.constructor.apply(this, arguments);
    }

    baseCls.prototype = {
        constructor: function (state) {
            console.log("constructor", arguments);

            this.subscribers = [];
            this.state = state || {};


            this.init.apply(this, arguments);
        },
        init: function () {

        },
        // state has to be Object or Array
        setState: function (state, silent) {
            // if state is an array, reference is replaced with the new state reference
            if (state instanceof Array) {
                this.state = state;
            } else {
                for (prop in state) {
                    this.state[prop] = state[prop];
                }
            }

            // dont update the ui, and dont notify anyone about the change
            if (silent !== true) {
                this._update();
            }

            return this;
        },
        // returns state
        getState: function () {
            return this.state;
        },
        // subscribe to state changes on a component
        onStateChange: function (callback, context) {
            if (typeof callback === "function") {
                this.subscribers.push({cb: callback, ctx: context || this});
            }

            return this;
        },
        // private function, calls render and notifies subscribers about the state change
        _update: function () {
            this.render();

            this.subscribers.map(function (conf) {
                conf.cb.apply(conf.ctx, this.state);
            });
        }
    };

    // add functions
    for (prop in cls) {
        baseCls.prototype[prop] = cls[prop];
    }

    // wrap render function
    baseCls.prototype._render = cls.render;

    baseCls.prototype.render = function () {

        var el = this._render();

        if (this.el !== undefined && this.el.parentNode !== undefined) {
            this.el.parentNode.replaceChild(el, this.el);
        }

        this.el = el;

        return el;
    };

    return baseCls;
}